import getGraphqlClient from "@/lib/getGraphqlClient.js";
import { getMe } from "@/lib/graphql/queries";

export default {
  namespaced: true,

  state: {
    services: {
      spotify: {
        authentication: {
          access_token: "",
          refresh_token: ""
        },
        me: null
      }
    }
  },

  getters: {},

  actions: {
    SET_SPOTIFY_TOKENS({ commit }, tokens) {
      commit("SET_SPOTIFY_TOKENS", tokens);
    },
    GET_ME({ commit }) {
      return getGraphqlClient()
        .query({
          query: getMe
        })
        .then(({ data: { me } }) => {
          commit("SET_ME", me);
        })
        .catch(err => {
          console.log(err);
        });
    }
  },

  mutations: {
    SET_SPOTIFY_TOKENS(state, { access_token, refresh_token }) {
      state.services.spotify.authentication.access_token = access_token
        ? access_token
        : state.services.spotify.authentication.access_token;

      state.services.spotify.authentication.refresh_token = refresh_token
        ? refresh_token
        : state.services.spotify.authentication.refresh_token;
    },
    SET_ME(state, me) {
      state.services.spotify.me = me;
    }
  }
};
