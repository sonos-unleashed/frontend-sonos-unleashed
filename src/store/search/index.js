import getGraphqlClient from "@/lib/getGraphqlClient.js";
import { getSearchResult } from "@/lib/graphql/queries";

export default {
  namespaced: true,

  state: {
    searchQuery: "",
    searchResult: {
      artists: null,
      tracks: null
    }
  },

  getters: {
    searchQuery: state => state.searchQuery
  },

  actions: {
    FETCH_SEARCH_RESULT({ commit }, query) {
      return getGraphqlClient()
        .query({
          query: getSearchResult,
          variables: {
            query: query
          }
        })
        .then(({ data: { search } }) => {
          commit("SET_SEARCH_RESULT", search);
        })
        .catch(err => {
          console.log(err);
        });
    }
  },

  mutations: {
    SET_SEARCH_QUERY(state, value) {
      state.searchQuery = value;
    },
    CLEAR_SEARCH_RESULT(state) {
      state.searchResult = {
        artists: null,
        tracks: null
      };
    },
    SET_SEARCH_RESULT(state, { artists, tracks }) {
      state.searchResult = {
        artists,
        tracks
      };
    }
  }
};
