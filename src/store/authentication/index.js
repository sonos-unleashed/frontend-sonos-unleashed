export default {
  namespaced: true,

  state: {
    spotify: {
      access_token: "",
      refresh_token: ""
    }
  },

  getters: {},

  actions: {
    SET_SPOTIFY_TOKENS({ commit }, tokens) {
      commit("SET_SPOTIFY_TOKENS", tokens);
    }
  },

  mutations: {
    SET_SPOTIFY_TOKENS(state, { access_token, refresh_token }) {
      state.spotify.access_token = access_token
        ? access_token
        : state.spotify.access_token;

      state.spotify.refresh_token = refresh_token
        ? refresh_token
        : state.spotify.refresh_token;
    }
  }
};
