import Vue from "vue";
import VueApollo from "vue-apollo";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store/index.js";
import ApolloClient from "apollo-boost";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faHome,
  faListOl,
  faMusic,
  faSearch,
  faUserAstronaut,
  faUser
} from "@fortawesome/free-solid-svg-icons";
import { faSpotify } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(
  faHome,
  faListOl,
  faMusic,
  faSearch,
  faUserAstronaut,
  faSpotify,
  faUser
);

Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.use(VueApollo);
const apolloClient = new ApolloClient({
  // You should use an absolute URL here
  uri: "http://localhost:9000/graphql",
  request: async operation => {
    const token = await localStorage.getItem("spotify_access_token");
    operation.setContext({
      headers: {
        authorization: token ? `Bearer ${token}` : ""
      }
    });
  }
});

//Framtida användning
// Vue.directive('scrolls', {
//   inserted: function (el, binding) {
//     let f = function (evt) {
//       if (binding.value(evt, el)) {
//         window.removeEventListener('scroll', f)
//       }
//     }
//     window.addEventListener('scroll', f)
//     console.log("hello")
//   }
// });

const apolloProvider = new VueApollo({
  defaultClient: apolloClient
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  apolloProvider,
  render: h => h(App)
}).$mount("#app");
