import Vue from "vue";
import Router from "vue-router";

import Artist from "@/pages/artists/_artist.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import("@/pages/index.vue")
    },
    {
      path: "/search",
      name: "search",
      // route level code-splitting
      // this generates a separate chunk (<chunk_name>.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "search" */ "@/pages/search/index.vue")
    },
    {
      path: "/playlists/",
      name: "playlists",
      component: () =>
        import(/* webpackChunkName: "search" */ "@/pages/playlists/index.vue")
    },
    {
      path: "/playlists/:id",
      name: "playlist",
      component: () =>
        import(/* webpackChunkName: "search" */ "@/pages/playlists/_playlist.vue")
    },
    {
      path: "/albums",
      name: "albums",
      component: () =>
        import(/* webpackChunkName: "search" */ "@/pages/albums/index.vue")
    },
    {
      path: "/albums/:id",
      name: "album",
      component: () =>
        import(/* webpackChunkName: "search" */ "@/pages/albums/_album.vue")
    },
    {
      path: "/artists/:id",
      name: "artist",
      component: Artist
    },
    {
      path: "/settings",
      name: "settings",
      component: () =>
        import(/* webpackChunkName: "settings" */ "@/pages/settings/index.vue")
    },
    {
      path: "/auth/spotify",
      name: "spotifyAuth",
      component: () => import("@/pages/auth/spotify.js")
    }
  ]
});
