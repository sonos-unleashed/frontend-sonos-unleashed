export default {
  render() {
    return;
  },
  mounted() {
    const access_token = this.$route.query.access_token;
    const refresh_token = this.$route.query.refresh_token;

    if (access_token && refresh_token) {
      localStorage.setItem(
        "spotify_access_token",
        this.$route.query.access_token
      );
      localStorage.setItem(
        "spotify_refresh_token",
        this.$route.query.refresh_token
      );

      this.$store
        .dispatch("user/SET_SPOTIFY_TOKENS", {
          access_token,
          refresh_token
        })
        .then(() => {
          this.$store.dispatch("user/GET_ME");
          this.$router.push({ name: "settings" });
        });
    }
  }
};
