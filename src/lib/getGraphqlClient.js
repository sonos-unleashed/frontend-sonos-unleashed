import { ApolloClient } from "apollo-client";
import { HttpLink } from "apollo-link-http";
import { setContext } from "apollo-link-context";
import { InMemoryCache } from "apollo-cache-inmemory";
import { customFetch } from "./apolloFetch";

function getApolloConfig() {
  const httpEndpoint = "http://localhost:9000" // eslint-disable-line

  const getStoredToken = () =>
    localStorage.getItem("spotify_access_token") || null;

  const getAuthToken = () => {
    return `Bearer ${getStoredToken()}`;
  };

  const httpLink = new HttpLink({
    uri: `${httpEndpoint}/graphql`,
    fetch: customFetch
  });

  const authLink = setContext((_, { headers }) => {
    return {
      headers: { ...headers, authorization: getAuthToken() }
    };
  });

  return {
    httpLink: authLink.concat(httpLink),
    cache: new InMemoryCache(),
    connectToDevTools: true // TODO check if prod
  };
}

export default getGraphqlClient();

function getGraphqlClient() {
  let client = null;

  return force => {
    if (!client || force) {
      const { httpLink, ...conf } = getApolloConfig();
      client = new ApolloClient({
        link: httpLink,
        ...conf
      });
    }

    return client;
  };
}
