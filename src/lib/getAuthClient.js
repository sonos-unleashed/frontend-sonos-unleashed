import axios from "axios";

export default axios.create({
  baseURL: "http://localhost:9000", // eslint-disable-line
  timeout: 5000
});
