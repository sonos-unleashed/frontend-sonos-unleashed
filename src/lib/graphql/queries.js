import gql from "graphql-tag";

export const getMe = gql`
  query {
    me {
      username
      albums {
        name
        id
        images {
          url
        }
      }
      tracks {
        name
        id
        album {
          name
          id
          images {
            url
          }
        }
      }
      playlists {
        name
        id
        owner {
          id
          display_name
        }
        images {
          url
        }
      }
    }
  }
`;

export const getPlaylist = gql`
  query($id: ID!) {
    playlist(id: $id) {
      id
      name
      owner {
        id
        display_name
      }
      tracks {
        id
        name
        artists {
          id
          name
        }
        album {
          id
          name
          images {
            url
          }
        }
      }
    }
  }
`;

export const getArtist = gql`
  query($id: ID!) {
    artist(id: $id) {
      id
      name
      images {
        url
      }
      topTracks {
        id
        name
      }
      albums {
        id
        album_type
        name
        images {
          url
        }
      }
    }
  }
`;

export const getAlbum = gql`
  query($id: ID!) {
    album(id: $id) {
      id
      name
      images {
        url
      }
      artists {
        id
        name
      }
      tracks {
        id
        name
        artists {
          id
          name
        }
      }
    }
  }
`;

export const getSearchResult = gql`
  query($query: String!) {
    search(query: $query) {
      artists {
        id
        images {
          url
        }
        name
      }
      tracks {
        id
        name
        album {
          id
          images {
            url
          }
        }
        artists {
          id
          name
        }
      }
    }
  }
`;
